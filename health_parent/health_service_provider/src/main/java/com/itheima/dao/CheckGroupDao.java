package com.itheima.dao;

import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;


public interface CheckGroupDao {

  /*  void add(CheckItem checkItem);*/

    List<CheckGroup> findPage(@Param("queryString") String queryString);

    void add(CheckGroup checkGroup);

    void setCheckGroupAndCheckItem(@Param("checkgroup_id") Integer checkgroup_id ,@Param("checkitem_id") List<Integer> checkitem_id);

    List<Integer> findCheckItemIdsByCheckGroupId(Integer id);

    void edit(CheckGroup checkGroup);

    void deleteAssociation(Integer id);

    void delete(Integer id);

/*    void change(CheckItem checkItem);

    void deleteById(Integer id);*/

}
