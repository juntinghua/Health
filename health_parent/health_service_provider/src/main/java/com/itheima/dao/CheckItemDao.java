package com.itheima.dao;

import com.itheima.entity.PageResult;
import com.itheima.pojo.CheckItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface CheckItemDao {

    void add(CheckItem checkItem);

    List<CheckItem> findPage(@Param("queryString") String queryString);

    void change(CheckItem checkItem);

    void deleteById(Integer id);

}
