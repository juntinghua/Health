package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.CheckGroupDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;
import com.itheima.service.CheckGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Service(interfaceClass = CheckGroupService.class)
@Transactional
public class CheckGroupServiceImpl implements CheckGroupService {

    @Autowired
    private CheckGroupDao checkGroupDao;

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        String queryString = queryPageBean.getQueryString();
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
       List<CheckGroup> page= checkGroupDao.findPage(queryString);
       PageInfo<CheckGroup> checkItemPageInfo = new PageInfo<CheckGroup>(page);
        PageResult pageResult = new PageResult(checkItemPageInfo.getTotal(), page);
        return pageResult;
    }

    @Override
    public void add(CheckGroup checkGroup,Integer[]checkitemIds) {
        checkGroupDao.add(checkGroup);
        Integer id = checkGroup.getId();

        List<Integer> integers = Arrays.asList(checkitemIds);


        checkGroupDao.setCheckGroupAndCheckItem(id,integers);
/*
        if(checkitemIds!=null&&checkitemIds.length>0){
            for (Integer checkitemId : checkitemIds) {
                HashMap<String, Integer> CheckGroupAndCheckItem = new HashMap<>();
                CheckGroupAndCheckItem.put("checkgroup_id", id);
                CheckGroupAndCheckItem.put("checkitem_id", checkitemId);
                checkGroupDao.setCheckGroupAndCheckItem(CheckGroupAndCheckItem);
            }
        }*/

    }

    @Override
    public List<Integer> findCheckItemIdsByCheckGroupId(Integer id) {
        return checkGroupDao.findCheckItemIdsByCheckGroupId(id);
    }

    @Override
    public void edit(CheckGroup checkGroup, Integer[] checkitemIds) {

        checkGroupDao.edit(checkGroup);
        Integer id = checkGroup.getId();

        checkGroupDao.deleteAssociation(id);
        List<Integer> integers = Arrays.asList(checkitemIds);
        checkGroupDao.setCheckGroupAndCheckItem(id,integers);

    }

    @Override
    public void deleteById(Integer id) {
        checkGroupDao.deleteAssociation(id);
        checkGroupDao.delete(id);
    }

 /*   @Override
    public void change(CheckItem checkItem) {
        checkItemDao.change(checkItem);
    }

    @Override
    public void deleteById(Integer id) {
        try {
            checkItemDao.deleteById(id);
        }catch (Exception e){
            throw e;
        }

    }

    @Override
    public List<CheckItem> findAll() {
        List<CheckItem> page = checkItemDao.findPage(null);
        return page;
    }*/

}
