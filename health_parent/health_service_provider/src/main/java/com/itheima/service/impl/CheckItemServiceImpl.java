package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.CheckItemDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckItemService;
import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = CheckItemService.class)
@Transactional
public class CheckItemServiceImpl implements CheckItemService {

    @Autowired
    private CheckItemDao checkItemDao;

    @Override
    public void add(CheckItem checkItem) {
        checkItemDao.add(checkItem);
    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        String queryString = queryPageBean.getQueryString();



        Page<CheckItem> page = PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()).doSelectPage(new ISelect() {
            @Override
            public void doSelect() {
                List<CheckItem> page = checkItemDao.findPage(queryString);
            }
        });
        // PageInfo<CheckItem> checkItemPageInfo = new PageInfo<CheckItem>(page);
        PageResult pageResult = new PageResult(page.getTotal(), page.getResult());
        return pageResult;
    }

    @Override
    public void change(CheckItem checkItem) {
        checkItemDao.change(checkItem);
    }

    @Override
    public void deleteById(Integer id) {
        try {
            checkItemDao.deleteById(id);
        }catch (Exception e){
            throw e;
        }

    }

    @Override
    public List<CheckItem> findAll() {
        List<CheckItem> page = checkItemDao.findPage(null);
        return page;
    }

}
